const websiteurl = window.location.href
const seubinver = JSON.parse('{"name":"Seubin Base", "ver":"1.3.1", "majspec":1, "minspec":3.1, "spec":1.3}');

const sbnfs = localStorage.getItem('seubinRoot');
const sbnfs_setupPath = localStorage.getItem('seubinFilesystemSetupPath');
const sbnfs_thingsPath = localStorage.getItem('seubinFilesystemThingsPath');

const sbn = {
    reboot : function() {
        window.location.href = "/";
    },

    bootbook : function() {
        if (!localStorage.getItem('seubinBootBook')) {
            sbn.reboot();
        } else {
            window.location.replace(localStorage.getItem('seubinBootBook'));
        }
    },

    launch : function(bookNameDir) {
        if (bookNameDir.includes(" setup") == true) {
            bookNameDir = bookNameDir.replace(" setup","");
            window.location.href = sbnfs + sbnfs_setupPath + "/" + bookNameDir + ".sbfs";
        } else if (bookNameDir.includes("/") == true) {
            window.location.href = bookNameDir;
        } else {
            window.location.href = sbnfs + sbnfs_thingsPath + bookNameDir + ".sbfs";
        }
    },

    themePref : function() {
        if (localStorage.getItem('seubinThemePref') == "aero") {
            if (!document.getElementById('sbnmajor') == "1" || document.querySelector('#sbnmajor') == "1" || document.querySelector('#sbnminor').innerHTML < "3.1") {
                    return "black";
            } else {
                return localStorage.getItem('seubinThemePref');
            }
        } else {
            return localStorage.getItem('seubinThemePref');
        }
    },

    setThemePref : function(themeVal) {
        const recThemes = [ //Themes
            "classic",
            "black",
            "white",
            "aero"
        ]
        
        if (recThemes.includes(themeVal)) {
            localStorage.setItem("seubinThemePref", themeVal);
            console.log("SET THEME AS: " + themeVal);
        } else {
            console.log("MISTAKE: Not a recognized theme");
        }
    },

    tungPref : function() {
        return localStorage.getItem('seubinTungPref');
    },

    setTungPref : function(seubinTung) {
        localStorage.setItem('seubinTungPref', seubinTung);
        console.log("SET TUNG AS: " + seubinTung);
    },

    iconPref : function() {
        return localStorage.getItem('seubinIconPref');
    },

    setIconPref : function(sizeIcon) {
        localStorage.setItem('seubinIconPref', sizeIcon);
        console.log("SET ICON PREF AS: " + sizeIcon);
    },

    setShellPref : function(shelldir) {
        localStorage.setItem('seubinBootBook', shelldir);
        console.log("SET SHELL PREF AS: " + shelldir);
    },

    shellPref : function() {
        return localStorage.getItem('seubinBootBook');
    },

    setGender : function(seubinGender) {
        localStorage.setItem('seubinGender', seubinGender);
        console.log("SET GENDER AS: " + seubinGender);
    },

    gender : function() {
        return localStorage.getItem('seubinGender');
    }
};

const sbn_theme = sbn.themePref();

if (document.querySelector("#sbnmajor").innerHTML > seubinver.majspec || document.querySelector("#sbnminor").innerHTML > seubinver.minspec) {
    console.log("MISTAKE: Book expects newer seubin version!");
    document.querySelector("body").innerHTML = "<p>MISTAKE: Book expects newer seubin version!</p> <br/> <p>BAD_SEUBIN_SPEC</p>";
}