const sbnpl = {
    uA : function() {
        return window.navigator.userAgent;
    },
    
    platform : function() {
        return localStorage.getItem("seubinPlatform");
    },
    
    setPlatform : function(platformstring) {
        const recPlatformKinds = [ //List of accepted platforms (keymouse = computer/laptop, touch = mobile, gamepad = steamdecks and its clones)
            "keymouse",
            "touch",
            "gamepad"
        ]
        
        if (recPlatformKinds.includes(platformstring)) {
            localStorage.setItem("seubinPlatform", platformstring);
        } else {
            console.log("MISTAKE: Not a recognized platform");
        }
    }
};
