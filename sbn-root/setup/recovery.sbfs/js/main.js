if (seubinver.spec < "1.3") {
    document.querySelector("body").innerHTML = "<p>Book failed to start, check the JavaScript Console for more details</p> <br/> <p>BAD_SEUBIN_SPEC</p>";
    alert("Wrong Specification, Expecting 1.3");
    console.log("MISTAKE: Book is expecting 1.3, but a different version is reported");
    stopAllExecution();
}

document.body.style.backgroundImage = "url('" + sbnfs + sbnfs_setupPath + "/seubin.sbfs/meting/artwork_" + sbn_theme + ".png')"; 
document.getElementById("theme").href = "./css/" + sbn_theme + ".css"

async function getTungJSON() {  // Get and load tung JSON
    try {
        const tjson = await fetch("./tungs/" + sbn.tungPref() + ".json");
        return await tjson.json();
    } catch (error) {
        const tjson = await fetch("./tungs/en.json"); //Download English json if the language's json cannot be downloaded
        return await tjson.json();
    }
}

async function bookMain() { // Main function
    const tung = await getTungJSON();
    document.querySelector("title").innerHTML = tung.recovery;
    document.getElementById("titlebar").innerHTML = tung.recovery;
    document.getElementById("btbkPtext").innerHTML = tung.bootbook;
    document.getElementById("setAsBtBkButt").innerHTML = tung.setAsBootBook;
    document.getElementById("resetRawputP").innerHTML = tung.resetRawput;
    document.getElementById("wipeLsBtn").innerHTML = tung.wipeLs;
    document.getElementById("wipeCkysBtn").innerHTML = tung.wipeCkys;
    document.getElementById("wipeSsBtn").innerHTML = tung.wipeSs;
    document.getElementById("wipeAllBtn").innerHTML = tung.wipeAll;
    document.getElementById("noItsLateAtNight").innerHTML = tung.no;
}

bookMain();

function setBootBook() {
    const bbField = document.getElementById("bootbookField")

    if (bbField.value == "/" || bbField.value == "/index.html" || bbField.value == "/setup/bootloader.sbfs" || bbField.value == "/setup/bootloader.sbfs/index.html") { // safenet the extremely bored idiots
        alert(document.getElementById("noItsLateAtNight").innerHTML);
    } else {
        sbn.setShellPref(document.getElementById("bootbookField").value);
    }
}

function wipeLocalStorage() {
    localStorage.clear();
}

function wipeSeubinSettings() {
    localStorage.removeItem("seubinBootBook");
    localStorage.removeItem("seubinThemePref");
    localStorage.removeItem("seubinGender");
    localStorage.removeItem("seubinIconPref");
    localStorage.removeItem("seubinTungPref");
    localStorage.removeItem("seubinBootstrapped");
    localStorage.removeItem("seubinSetupEnded");
    sbn.reboot();
}

function wipeCookies() {
    katze.rmCkys
}

function wipeAll() {
    wipeCookies();
    wipeLocalStorage();
}
