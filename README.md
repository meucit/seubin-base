# seubin-base

Seubin Base is an operating environment for your webbrowser that launches seubin books (folders ending in .sbfs) and it also has small APIs (like the Seubin API and libkatze)

To build a rom for Wrap run ./build.sh rom

CJK fonts will heavily increase the file size of the built rom, if you don't need them (or can trust your clients have them installed already) build with SBN_BUILD_CJK=0 ./build.sh rom