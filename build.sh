#!/usr/bin/env bash

if [[ $SBN_BUILD_CJK = "" ]]; then
    export SBN_BUILD_CJK=0
fi

if [[ $1 = "clean" ]]; then
    rm -rf ./build
elif [[ $1 = "rom" ]]; then
    if [[ -d ./build/sbn-root ]]; then
        cd ./build
        zip -r ../seubin-base.1.3.1.seubin ./
    else
        ./build.sh
        ./build.sh rom
    fi
else
    if [[ $SBN_BUILD_CJK = "1" ]]; then
        if [[ -d "./sbn-root/setup/seubin.sbfs/fonts/notoserif" ]]; then
            echo "Found Notoserif CJK fonts"
        else
            echo "CJK wasn't found, please clone the seubin-cjk repository and paste notoserif folder into sbn-root/setup/seubin.sbfs/fonts"
            export SBN_BUILD_CJK=0
        fi
    fi
    mkdir ./build
    mkdir ./build/sbn-root
    mkdir ./build/sbn-root/setup
    mkdir ./build/sbn-root/setup/settings.aso
    mkdir ./build/sbn-root/things
    mkdir ./build/sbn-root/ware
    cp -a ./sbn-root/* ./build/sbn-root
    rm ./build/sbn-root/README.md # Delete unneeded files on prod
    rm -r ./build/sbn-root/setup/seubin.sbfs/meting/*.xcf
    rm -r ./build/sbn-root/setup/seubin.sbfs/meting/*.kra
    rm -r ./build/sbn-root/setup/seubin.sbfs/meting/*.comment
    if [[ $SBN_BUILD_CJK = "0" ]]; then
        rm -r ./build/sbn-root/setup/seubin.sbfs/fonts/notoserif
    else
        mv ./build/sbn-root/setup/seubin.sbfs/fonts/notoserif ./build/sbn-root/setup/seubin.sbfs/fonts/cjk
    fi
    mv ./build/sbn-root/setup/seubin.sbfs/fonts/opensans ./build/sbn-root/setup/seubin.sbfs/fonts/latin-cyrillic
    touch ./build/NEEDED_WRAP_VERSION
    echo "5" > ./build/NEEDED_WRAP_VERSION # Bump up Wrap requirement to 0.3.3+
fi
